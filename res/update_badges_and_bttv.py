import json
import sys
import urllib.parse
import urllib.request
from urllib.error import HTTPError

with urllib.request.urlopen('https://badges.twitch.tv/v1/badges/global/display?language=en') as global_badges_response:
    badge_json = json.loads(global_badges_response.read())

print("What is the channel name?", file=sys.stderr)
channel_name = input()

channel_id_url = 'https://api.twitch.tv/kraken/users?login={}'.format(channel_name)
channel_id_headers = {
    'Accept': 'application/vnd.twitchtv.v5+json',
    'Client-ID': 'isaxc3wjcarzh4vgvz11cslcthw0gw'
}
channel_id_req = urllib.request.Request(
    channel_id_url, headers=channel_id_headers)
with urllib.request.urlopen(channel_id_req) as channel_id_response:
    channel_id = json.loads(channel_id_response.read())['users'][0]['_id']

channel_badges_url = 'https://badges.twitch.tv/v1/badges/channels/{}/display?language=en'.format(channel_id)
with urllib.request.urlopen(channel_badges_url) as channel_badges_response:
    subscriber_json = json.loads(
        channel_badges_response.read())['badge_sets']['subscriber']
badge_json['badge_sets']['subscriber'] = subscriber_json

with open("res/badges.json", "w") as badge_file:
    badge_file.write(json.dumps(badge_json))

bttv_url = 'https://api.betterttv.net/2/channels/{}'.format(channel_name)
bttv_text = '{"emotes": []}'
try:
    with urllib.request.urlopen(bttv_url) as bttv_response:
        bttv_text = bttv_response.read().decode('utf-8')
except HTTPError as e:
    if e.code != 404:
        raise e

with open("res/bttv_channel.json", "w") as badge_file:
    badge_file.write(bttv_text)
